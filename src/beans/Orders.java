package beans;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;
import java.util.ArrayList;
import model.MyOrder;
import javax.inject.Inject;
import business.OrdersBusinessInterface;

@ManagedBean(name="orders")
@ViewScoped
public class Orders {
	
	@Inject OrdersBusinessInterface service;

	List<MyOrder> orders;
	
	public Orders() {
		
	}
	
	@PostConstruct
	public void init() {
		orders = service.getOrders();
	}
	
	public List<MyOrder> getOrders() {
		return orders;
	}

	public void setOrders(List<MyOrder> orders) {
		this.orders = orders;
	}
}
