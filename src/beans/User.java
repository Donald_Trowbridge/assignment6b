package beans;

import java.security.Principal;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.validation.constraints.Size;
import javax.validation.constraints.NotNull;

@ManagedBean
@SessionScoped
public class User {
		
		@NotNull()
		@Size(min=4, max=15)
		protected String firstName;
		
		@NotNull()
		@Size(min=4, max=15)
		protected String lastName;
		
		public User() {
			firstName = "Donald";
			lastName = "Trowbridge";
		}
		
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		
		@PostConstruct
		public void init() {
			Principal principle = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal();
			
			if(principle == null) {
				setFirstName("Unknown");
				setLastName("");
			} else {
				setFirstName(principle.getName());
				setLastName("");
			}
		}
}
