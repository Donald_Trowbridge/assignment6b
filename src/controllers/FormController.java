package controllers;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import beans.User;
import business.OrdersBusinessInterface;
import model.MyOrder;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import javax.ejb.EJB;
import business.MyTimerService;

@ManagedBean(name="formController")
@ViewScoped
public class FormController {
	
	@Inject OrdersBusinessInterface services;
	@EJB MyTimerService timer;
	@PersistenceContext(unitName="assignment6b") EntityManager em;

	public String onLogoff() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		
		return "TestResponse.xhtml?faces-redirect=true";
	}
	
	public String onFlash(User user) {
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("User", user);
		return "TestResponse2.xhtml?faces-redirect=true";
	}
	
	public OrdersBusinessInterface getServices() {
		return services;
	}
	
	public void redirectToTestResponse() throws IOException {
		FacesContext.getCurrentInstance().getExternalContext().redirect("TestResponse.xhtml");
	}
	
	public String onSendOrder() {
		MyOrder order = new MyOrder();
		order.setOrderNo("02364");
		order.setProductName("Desktop");
		order.setPrice(new BigDecimal("899.99"));
		order.setQuantity(1);
		services.sendOrder(order);
		return "OrderResponse.xhtml";
	}
}
