package business;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
import beans.Order;
import business.OrdersBusinessInterface;
import javax.inject.Inject;
import model.MyOrder;

@RequestScoped
@Path("/orders")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
public class OrdersRestService {

	@Inject OrdersBusinessInterface service;
	
	@GET
	@Path("/getjson")
	@Produces(MediaType.APPLICATION_JSON)
	public List<MyOrder> getOrdersAsJson(){
		return service.getOrders();
	}
	
	@GET
	@Path("/getxml")
	@Produces(MediaType.APPLICATION_XML)
	public MyOrder[] getOrdersAsXml() {
		List<MyOrder>orders = service.getOrders();
		Object[]objArr = orders.toArray();
		MyOrder[] orderArr = new MyOrder[objArr.length];
		for(int i = 0; i < objArr.length; i++) {
			orderArr[i] = (MyOrder)objArr[i];
		}
		return orderArr;
	}
}
