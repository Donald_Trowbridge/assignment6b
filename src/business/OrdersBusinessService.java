package business;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import java.util.List;
import model.MyOrder;
import javax.annotation.Resource;
import javax.ejb.EJB;

/**
 * Session Bean implementation class OrdersBusinessService
 */
@Stateless
@Local(OrdersBusinessInterface.class)
@LocalBean
@Alternative
public class OrdersBusinessService implements OrdersBusinessInterface {

	protected List<MyOrder> orders;
	@EJB OrdersDataService service;
	@Resource(mappedName="java:/ConnectionFactory") private ConnectionFactory connectionFactory;
	@Resource(mappedName="java:/jms/queue/Order") private Queue queue;
    /**
     * Default constructor. 
     */
    public OrdersBusinessService() {
        // TODO Auto-generated constructor stub

    }

	/**
     * @see OrdersBusinessInterface#test()
     */
 
    public void setOrders(List<MyOrder> orders) {
    	this.orders = orders;
    }
    
    public List<MyOrder> getOrders(){
    	return service.findAll();
    }
    
    public void sendOrder(MyOrder order) {
    	
    	try{
    		Connection connection = connectionFactory.createConnection();
    		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
    		MessageProducer messageProducer = session.createProducer(queue);
    		
    		TextMessage message1 = session.createTextMessage();
    		message1.setText("This is text message");
    		messageProducer.send(message1);  		
    		
    		
    		ObjectMessage objMessage = session.createObjectMessage();
    		objMessage.setObject(order);
    		messageProducer.send(objMessage);
    		
    		connection.close();
    	}catch(JMSException e) {
    		e.printStackTrace();
    	}
    }

}
