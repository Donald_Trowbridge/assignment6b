package business;

import javax.ejb.Local;
import java.util.List;
import model.MyOrder;

@Local
public interface OrdersBusinessInterface {

	public List<MyOrder> getOrders();
	
	public void setOrders(List<MyOrder> orders);
	
	public void sendOrder(MyOrder order);
}
